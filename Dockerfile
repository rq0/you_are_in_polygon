# Dockerfile
# Pull base image
FROM python:3.8
# Set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
# Set work directory
WORKDIR /code
# Install dependencies
RUN apt-get update &&\
    apt-get install -y binutils libproj-dev gdal-bin libsqlite3-mod-spatialite
RUN pip install pipenv
COPY Pipfile Pipfile.lock /code/
RUN pipenv install --system


# Copy project
COPY . /code/