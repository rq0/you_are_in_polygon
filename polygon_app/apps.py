from django.apps import AppConfig


class PolygonAppConfig(AppConfig):
    name = 'polygon_app'
