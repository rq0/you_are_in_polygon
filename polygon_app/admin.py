from django.contrib.gis import admin
from .models import Polygon

admin.site.register(Polygon, admin.OSMGeoAdmin)
