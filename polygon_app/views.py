from django.contrib.gis.geos import Point
from django.http import JsonResponse, Http404
from django.views.generic import TemplateView

from polygon_app.models import Polygon


class PolygonView(TemplateView):
    template_name = 'polygon.html'

    def get_context_data(self, **kwargs):
        ctx = super(PolygonView, self).get_context_data(**kwargs)
        try:
            ctx['polygon'] = Polygon.objects.get(
                pk=kwargs['pk'])
        except Polygon.DoesNotExist:
            raise Http404
        return ctx

    def post(self, request, *args, **kwargs):
        point = Point(
            float(request.POST.get('x')),
            float(request.POST.get('y')))
        polygon = Polygon.objects.get(
            pk=kwargs['pk'])
        return JsonResponse({
            'in_polygon': polygon.poly.contains(point)
        })
